import math
import json

from web3 import Web3

LEFT_SIDE = 0
RIGHT_SIDE = 1
EMPTY_HASH = '0x00'

class Node:

    def __init__(self, hash='', content=None):
        self.hash = hash
        self.content = content
        
        self.is_leaf = True if content else False
        self.parent = None
        self.side = None

        self.left_node = None
        self.right_node = None
        self.left_size = 0
        self.right_size = 0
    
    def is_balanced(self):
        return (self.right_size == self.left_size)

class MerkleTree:

    def __init__(self):
        self.root_node = None
        self.aux_node = None
        self.last_node = None
        self.hash_table = {}
        self.hash_list = []
        self.last_committed = 0

    def add_node(self, content):
        hash = Web3.sha3(text=content).hex()
        content_node = Node(hash, content)

        self.hash_list.append(hash)
        self.hash_table[hash] = content_node


        if self.root_node is None:
            self.root_node = content_node
            self.last_node = content_node
            self.aux_node = content_node

            return content_node.hash
        
        node = Node()
        node.right_node = content_node
        content_node.parent = node
        content_node.side = RIGHT_SIDE

        if self.root_node.is_leaf or self.root_node.is_balanced():
            node.left_node = self.root_node
            self.root_node.side = LEFT_SIDE
            self.root_node.parent = node
            self.root_node = node
            self.aux_node = self.root_node
        else:
            if self.aux_node.is_balanced():
                node.parent = self.aux_node.parent
                self.aux_node.parent.right_node = node
                node.side = RIGHT_SIDE
                node.left_node = self.aux_node
                self.aux_node.side = LEFT_SIDE
                self.aux_node.parent = node
            else:
                node.left_node = self.aux_node.right_node
                self.aux_node.right_node.side = LEFT_SIDE
                node.left_node.parent = node
                node.parent = self.aux_node
                self.aux_node.right_node = node
                node.side = RIGHT_SIDE 
            
            self.aux_node = node
        
        # Re-caculating sizes
        p = self.aux_node
        while p:
            hash_to_update =  p.left_node.hash
            if p.right_node:
                hash_to_update = hash_to_update + p.right_node.hash.replace('0x', '')
            
            hash = Web3.sha3(hexstr=hash_to_update).hex()
            p.hash = hash

            if p.left_node:
                p.left_size = p.left_node.left_size + p.left_node.right_size + 1
            if p.right_node:
                p.right_size = p.right_node.left_size + p.right_node.right_size + 1
            if p.is_balanced():
                self.aux_node = p
            p = p.parent
        
        return content_node.hash

    def get_sync_data(self):
        length = len(self.hash_list)
        proofs, sides = self.get_proof_of_consistency(self.last_committed, 0, length, True)
        self.last_committed = length
        return self.root_node.hash, proofs, sides

    def get_proof_of_consistency(self, m, first, last, b):
        n = last - first
        k = int(math.floor(2**(math.ceil(math.log2(n) - 1))))

        if m == n:
            if b:
                return [EMPTY_HASH], [0]
            else:
                return [self.get_mth(first, last)], [0]
        else:
            if k < 1:
                return [], []
            elif m <= k:
                proofs, sides = self.get_proof_of_consistency(m, first, first + k, b)
                proofs.append(self.get_mth(first + k, last))
                sides.append(RIGHT_SIDE)
                return proofs, sides
            else:
                proofs, sides = self.get_proof_of_consistency(m - k, first + k, last, False)
                proofs.append(self.get_mth(first, first + k))
                sides.append(LEFT_SIDE)
                return proofs, sides

    def get_proof_of_membership(self, hash):
        node = self.hash_table[hash]
        if not node:
            return
        content = json.loads(node.content)
        r = {'content': content, 'proofs':[], 'sides':[]}
        
        c = 0
        p = node
        q = node.parent
        while q:
            c += 1
            if p.side == RIGHT_SIDE:
                r['sides'].append(LEFT_SIDE)
                r['proofs'].append(q.left_node.hash)
            else:
                r['sides'].append(RIGHT_SIDE)
                r['proofs'].append(q.right_node.hash)
            p = q
            q = p.parent
        return json.dumps(r)


    def get_mth(self, first, last):
        k = int(math.ceil(math.log2(last - first)))
        hash = self.hash_list[first]
        p = self.hash_table[hash]
        while k > 0:
            p = p.parent
            k = k - 1
        if not p:
            return None
        return p.hash
    
    def verify_proof(self, prev_root_hash, root_hash, proofs, sides):
        if proofs[0] == EMPTY_HASH:
            proofs[0] = prev_root_hash
        hash_new = proofs[0]
        hash_old = proofs[0]
        for i in range(1, len(proofs)):
            if sides[i] == RIGHT_SIDE:
                hash_new = Web3.sha3(hexstr=(hash_new + proofs[i].replace('0x', ''))).hex()
            else:
                hash_new = Web3.sha3(hexstr=(proofs[i] + hash_new.replace('0x', ''))).hex()
                hash_old = Web3.sha3(hexstr=(proofs[i] + hash_old.replace('0x', ''))).hex()
        return hash_new, hash_old