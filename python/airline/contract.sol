pragma solidity ^0.4.25;

contract Provider {

    bytes32 root;
    bytes32[] prev_roots;

    constructor() public {}

    function validate_membership(string memory json, bytes32[] memory proofs, uint8[] memory sides)
    public view returns(bool) {
        bytes32 ignored;
        bytes32 hash = keccak256(json);

        (hash, ignored) = get_mth(proofs, sides, hash);

        return (hash == root);
    }

    function validate_consistency(bytes32 hash, bytes32[] memory proofs, uint8[] memory sides)
    public {
        bytes32 hash_new;
        bytes32 hash_old;
        if (root == 0x00) {
            root = hash;
        }
        else{
            if (proofs[0] == 0x00) {proofs[0] = root;}

            (hash_new, hash_old) = get_mth(proofs, sides, 0x00);

            if ((hash_new == hash) && (hash_old == root)) {
                root = hash_new;
                prev_roots.push(hash_old);
            }
        }
    }

    function get_mth(bytes32[] proofs, uint8[] sides, bytes32 leaf)
    private pure returns(bytes32, bytes32) {
        uint i = 0;
        bytes32 hash_new = leaf;
        bytes32 hash_old = leaf;
        if (leaf == 0x00) {
            i = 1;
            hash_new = proofs[0];
            hash_old = proofs[0];
        }
        for (i; i < proofs.length; i++) {
            if (sides[i] == 1) {
                hash_new = keccak256(hash_new, proofs[i]);
            } else {
                hash_old = keccak256(proofs[i], hash_old);
                hash_new = keccak256(proofs[i], hash_new);
            }
        }
        return (hash_new, hash_old);
    }

    function get_root() public view returns(bytes32) {
        return root;
    }

    function get_prev_roots() public view returns(bytes32[]) {
        return prev_roots;
    }

}
