import json
import qrcode
import merkletree

import pandas as pd

from flask import Flask, render_template, request, redirect, jsonify, send_file

from solc import compile_source
from web3.auto import w3
from web3.middleware import geth_poa_middleware

all_data_df = None
submitted = False

app = Flask(__name__)

mk = merkletree.MerkleTree()

contract_source_code = None
contract_source_code_file = 'contract.sol'

with open(contract_source_code_file, 'r') as file:
    contract_source_code = file.read()

# Compile the contract
contract_compiled = compile_source(contract_source_code)
contract_interface = contract_compiled['<stdin>:Provider']

# Set the default account
w3.eth.defaultAccount = w3.eth.accounts[2]
#w3.personal.unlockAccount(w3.eth.accounts[0], '000')
#w3.middleware_stack.inject(geth_poa_middleware, layer=0)

# Contract abstraction
Provider = w3.eth.contract(abi=contract_interface['abi'], bytecode=contract_interface['bin'])

# Create an instance, i.e., deploy on the blockchain
tx_hash = Provider.constructor().transact()
tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

# Contract Object
provider = w3.eth.contract(address=tx_receipt.contractAddress, abi=contract_interface['abi'])

###
config = {'contract_addr': provider.address}
mk.add_node('config') ## Not manifest 
first_root, proofs, sides = mk.get_sync_data()
tx_hash = provider.functions.validate_consistency(first_root, proofs, sides).transact()
w3.eth.waitForTransactionReceipt(tx_hash)
###

flights_data = {'0x5fbcc2a0b30445db2fb5b01280bbb59c6b2621e6': 
                {'hash': '0x3d5b38e7c4111e0c7818bc14bbf91510bb43015453440cdf023a2356f5f7ae6c',
                 'content': {'Year': 2008,
                            'Month': 1,
                            'DayofMonth': 27,
                            'CRSDepTime': 1935,
                            'Origin': 'SLC',
                            'Dest': 'OKC'}},
                '0xa18fe04f02365e0d31514b276e209ec02f34034e': 
                {'hash': '0xa6154cf75237eeda99c0dd079dda1c6728b0f9368a76ef26943ab362d1a32bee',
                 'content': {'Year': 2008,
                            'Month': 1,
                            'DayofMonth': 29,
                            'CRSDepTime': 1935,
                            'Origin': 'SLC',
                            'Dest': 'OKC'}}}
        

@app.route('/')
@app.route('/index.html')
def index():
    return render_template('index.html')

@app.route('/manifest.json')
def manifest():
    return jsonify(config)

@app.route('/manage.html')
def manage():
    return render_template('manage.html')

@app.route('/admin.html')
def admin():
    global submitted
    return render_template('admin.html', 
                           submitted=submitted, 
                           dataframe=(all_data_df.to_html() if submitted else None),
                           root_hash=(mk.root_node.hash if submitted else None))

@app.route('/load.html', methods=['POST'])
def load_data():
    global submitted
    global all_data_df
    if request.method =='POST':
        file = request.files['flights']
        if file:
            columns = ['Year', 'Month', 'DayofMonth', 'CRSDepTime', 'Origin', 'Dest', 'ArrDelay', 'Cancelled']
            data = pd.read_csv(file)
            df = data[columns]
            # Load in merkle tree
            for _, row in df.iterrows():
                if row['Cancelled'] == 1:
                    row['ArrDelay'] = 0
                data = json.dumps(row.to_dict())
                mk.add_node(data)
            all_data_df = df
            submitted = True
    return redirect('admin.html')

@app.route('/submit.html')
def submit_root():
    global submitted
    global provider
    if submitted:
        root_hash, proofs, sides = mk.get_sync_data()
        tx_hash = provider.functions.validate_consistency(root_hash, proofs, sides).transact()
        w3.eth.waitForTransactionReceipt(tx_hash)
        submitted = False
    return redirect('admin.html')

@app.route('/flights.html')
def flights():
    account = request.args.get('account')
    hash = flights_data[account]['hash']

    content = flights_data[account]['content']
    qr_content = json.dumps(content)
    flights_data[account]['image'] = "to_buy.jpg"
    if hash in mk.hash_table:
        qr_content = mk.get_proof_of_membership(hash)
        content = json.loads(mk.hash_table[hash].content)
        flights_data[account]['image'] = "to_claim.jpg"
    
    qr = qrcode.QRCode(version = 1,
                       error_correction = qrcode.constants.ERROR_CORRECT_H,
                       box_size = 10,
                       border = 4)
    
    qr.add_data(qr_content)
    qr.make(fit=True)
    img = qr.make_image(fill_color="black", back_color="white")
    img.save(account + flights_data[account]['image'])
    
    return jsonify(content)

@app.route('/get_image.jpg')
def get_image():
    account = request.args.get('account')
    if account in flights_data:
        return send_file(account + flights_data[account]['image'], mimetype='image/jpg')
    return ('', 204)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)