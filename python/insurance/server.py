import json
import requests
import merkletree as mk

from PIL import Image

from pyzbar.pyzbar import decode

from flask import Flask, render_template, request, redirect, jsonify, send_file

from solc import compile_source
from web3.auto import w3 
from web3.middleware import geth_poa_middleware
from web3 import Web3

insurance_list = {}

resp = requests.get('http://localhost:8080/manifest.json')
config = resp.json()
provider_addr = config['contract_addr']

contract_source_code = None
contract_source_code_file = 'contract.sol'

with open(contract_source_code_file, 'r') as file:
    contract_source_code = file.read()

# Compile the contract
contract_compiled = compile_source(contract_source_code)
contract_interface = contract_compiled['<stdin>:Insurance']

# Set the default account
w3.eth.defaultAccount = w3.eth.accounts[2]
#w3.personal.unlockAccount(w3.eth.accounts[0], '000')
#w3.middleware_stack.inject(geth_poa_middleware, layer=0)

# Contract abstraction
Insurance = w3.eth.contract(abi=contract_interface['abi'], bytecode=contract_interface['bin'])

# Create an instance, i.e., deploy on the blockchain
tx_hash = Insurance.constructor(provider_addr).transact()
tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

# Contract Object
insurance = w3.eth.contract(address=tx_receipt.contractAddress, abi=contract_interface['abi'])
tx_hash = insurance.functions.deposit().transact({'value':Web3.toWei(0.23, 'ether')})
w3.eth.waitForTransactionReceipt(tx_hash)

app = Flask(__name__)

@app.route('/')
@app.route('/index.html')
def index():
    return render_template('index.html')

@app.route('/wipe.html')
def wipe():
    account = request.args.get('account')
    tx = request.args.get('tx')
    if account in insurance_list:
        del insurance_list[account]
        if tx and account:
            w3.eth.waitForTransactionReceipt(tx)
            status = insurance.functions.get_hash(Web3.toChecksumAddress(account)).call()
            print(status)
    return redirect('home.html')

@app.route('/home.html')
def home():
    account = request.args.get('account')
    tx = request.args.get('tx')

    content = {}
    status = None
    if account in insurance_list:
        content = insurance_list[account]
    if tx and account:
        w3.eth.waitForTransactionReceipt(tx)
        status = insurance.functions.get_hash(Web3.toChecksumAddress(account)).call()
    return render_template('home.html', **content, status=status)

@app.route('/claim.html', methods=['GET'])
def claim():
    return render_template('claim.html')

@app.route('/claim.html', methods=['POST'])
def read_claim():
    if request.method =='POST':
        file = request.files['qr']
        if file:
            decoded_qr = decode(Image.open(file))
            json_data = json.loads(decoded_qr[0].data)
            content = json.dumps(json_data['content'])
            proofs = json_data['proofs']
            sides = json_data['sides']
    return render_template('claim.html', content=content, proofs=proofs, sides=sides, address=insurance.address, abi=json.dumps(contract_interface['abi']))

@app.route('/buy.html', methods=['GET'])
def buy():
    return render_template('buy.html')

@app.route('/buy.html', methods=['POST'])
def read_buy():
    if request.method =='POST':
        file = request.files['qr']
        account = request.form['account']
        if file:
            decoded_qr = decode(Image.open(file))
            json_data = json.loads(decoded_qr[0].data)
            content = json.dumps(json_data)
            hash = Web3.sha3(text=content).hex()

            insurance_list[account] = json_data
            return render_template('buy.html', content=content, hash=hash, address=insurance.address, abi=json.dumps(contract_interface['abi']))
    return redirect('buy.html')

@app.route('/insurance.json')
def insurance_json():
    account = request.args.get('account')
    content = None
    if account in insurance_list:
        content = insurance_list[account]
    return jsonify(content)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)