import json
import requests
import binascii

from cryptography import x509
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256

from web3 import Web3, HTTPProvider
from web3.contract import ConciseContract

CONTRACT_ADDRESSES = '0x...'
CONTRACT_ABI = "Parties.abi"
ACCOUNT_PASSWORD = '000'

FIXTURE_ID = '1048570'

def main():
    if verifying_signature():
        w3 = Web3(Web3.HTTPProvider("http://localhost:8545"))
        w3.personal.unlockAccount(w3.eth.accounts[0], ACCOUNT_PASSWORD)

        content, proofs, sides, length = get_data_to_submit(FIXTURE_ID)

        ###################################################################
        ### Contract parties mibehavior simulation
        ###################################################################
        #proofs[0] = bytes.fromhex('01' * 32)
        ###################################################################

        abi = json.load(open(CONTRACT_ABI))

        tx = None
        receipt = None

        contract_instance = w3.eth.contract(address=CONTRACT_ADDRESSES, abi=abi, ContractFactoryClass=ConciseContract)

        tx = contract_instance.resetValidated(transact={'from': w3.eth.accounts[0]})

        tx = contract_instance.submitUltimateData(content, proofs, sides, transact={'from': w3.eth.accounts[0]})

        w3.eth.waitForTransactionReceipt(tx)
        receipt = w3.eth.getTransactionReceipt(tx)
        tx = "0x" + binascii.hexlify(tx).decode("utf-8")

        print("Data submited")
        print("Transaction", tx)
        print("Data", type, len(proofs), length)

        w3.personal.lockAccount(w3.eth.accounts[0])

def get_data_to_submit(id):
    req = requests.get('https://localhost/fixtures/' + id, verify=False)
    fixture = req.json()
    content = json.dumps(fixture['content']).replace(': ', ':').replace(', ', ',').encode('utf-8')

    proofs = []
    sides = []

    for proof in fixture['proof']:
        proofs.append(bytes.fromhex(proof['hash']))
        sides.append(proof['side'])

    return content, proofs, sides, fixture['length']

def verifying_signature():
    #########################################################
    # Loading certificate got from HTTPS service
    #########################################################
    with open('PATH TO CERTIFICATE (DER FORMAT)', 'rb') as f:
        cert = f.read()
    _x509 = x509.load_der_x509_certificate(cert, default_backend())
    public_key = _x509.public_key()

    pem = public_key.public_bytes(encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo)
    key = RSA.importKey(pem)

    req = requests.get('https://localhost/config.json', verify=False)
    config = req.json()
    signature = bytes.fromhex(config['signature'])

    # Signature verification
    json_to_hash = json.dumps(config['signed']).replace(': ', ':').replace(', ', ',').encode('utf-8')
    hash_to_verify = SHA256.new(json_to_hash)

    verifier = PKCS1_v1_5.new(key)

    if verifier.verify(hash_to_verify, signature):
        print("===== Signed Merkle Tree Configuration =====")
        print("Smart Contract ID/Address:", config['signed']['sc_address'])
        print("URL/API/JSON:", config['signed']['url'])
        print("Expires on:", config['signed']['expiration'])
        print("Frequency of updating:", config['signed']['frequency'])
        print("Data structure:")
        print(config['signed']['data_structure'])
        print("===== Extra configuration - non-signed =====")
        print("Notes:", config['notes'])
        return True
    else:
        print("***** Merkle Tree Configuration Signed Wrongly *****")
        return False

if __name__ == "__main__":
    main()
