pragma solidity ^0.4.21;

import "./imported/JsmnSolLib.sol";
import "./imported/Provider.sol";

contract Parties {

    bool validated;
    address receiver;

    Provider provider = Provider(0x5c6De721F6F8C21e313C71090b8DBD646CC9689D);

    function Parties() public {}

    function receiveMoney() payable public returns (bool success) {
        receiver = msg.sender;
        return true;
    }

    function sendMoney() public {
        receiver.transfer(100000000000000000);
    }

    function submitUltimateData(string json, bytes32[] proofs, uint8[] sides)
    external {
        validated = provider.validateProofOfExistence(json, proofs, sides);

        if (validated) {
            bool parsed;

            parsed = parseJSONdata(json);

            if (parsed) {
                sendMoney();
            } else {
                sendMoney();
            }
        }
    }

    function parseJSONdata(string json) pure public returns (bool) {
        string memory id;
        string memory local;
        string memory visitor;
        uint8 localGoals;
        uint8 visitorGoals;

        uint res;
        uint numTokens;

        JsmnSolLib.Token memory t;
        JsmnSolLib.Token[] memory tokens;

        (res, tokens, numTokens) = JsmnSolLib.parse(json, 50);
        require(res == 0);

        t = tokens[2];
        id = JsmnSolLib.getBytes(json, t.start, t.end);

        t = tokens[4];
        local = JsmnSolLib.getBytes(json, t.start, t.end);

        t = tokens[6];
        visitor = JsmnSolLib.getBytes(json, t.start, t.end);

        t = tokens[8];
        localGoals = uint8(JsmnSolLib.parseInt(JsmnSolLib.getBytes(json, t.start, t.end)));

        t = tokens[10];
        visitorGoals = uint8(JsmnSolLib.parseInt(JsmnSolLib.getBytes(json, t.start, t.end)));

        return true;
    }

    function resetValidated() public {
        validated = false;
    }

    function getValidated() view public returns(bool) {
        return validated;
    }

}
