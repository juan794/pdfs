package merkletree

type JSONNode struct {
    Id string `json:"id"`
    Hash string `json:"hash"`
    IdParent string `json:"parent"`
    Type uint `json:"type"`
    Content interface{} `json:"content"`
    TreeLength uint `json:"length"`
    Proof []Proof `json:"proof"`
}

type Proof struct {
    Side uint `json:"side"`
    Hash string `json:"hash"`
}
