package merkletree

import(
    "log"
    "time"
    "math"
    "bytes"
    "errors"

    "io/ioutil"

    "encoding/pem"
    "encoding/hex"
    "encoding/json"

    "crypto"
    "crypto/sha256"
    "crypto/x509"
    "crypto/rand"
    "crypto/rsa"

    ethercrypro "github.com/ethereum/go-ethereum/crypto"
    "github.com/ethereum/go-ethereum/common"
)

type MerkleTree struct {
    Root *Node
    Aux *Node
    Last *Node
    Config *Config
    List []*Node
    LastCommitted uint
}

func Init(keyFile string, certFile string, url string, scAddress string, expiration_date string) (*MerkleTree, error) {
    mk := new(MerkleTree)

    configNode, configJSON, err := getConfigAndSignContent(keyFile, certFile, url, scAddress, expiration_date)
    if err != nil {
        log.Fatal(err)
        return nil, errors.New("No configuration for merkle tree")
    }

    mk.Config = configNode
    mk.List = make([]*Node, 0, 1)
    mk.AddNewNode(configJSON, 2)
    return mk, nil
}

func getConfigAndSignContent(keyFile string, certFile string, url string, scAddress string, expiration_date string) (*Config, []byte, error) {
    rng := rand.Reader
    privateKeyData, _ := ioutil.ReadFile(keyFile)
    block, _ := pem.Decode([]byte(privateKeyData))
    if block == nil || block.Type != "RSA PRIVATE KEY" {
        log.Fatal("failed to decode PEM block containing public key")
        return nil, nil, errors.New("No merkle tree")
    }
    rsaKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
    if err != nil {
        log.Fatal(err)
        return nil, nil, errors.New("No merkle tree")
    }

    configNode := new(Config)
    configNode.Signed.URL = url
    configNode.Signed.SmartContractAddress = scAddress
    configNode.Signed.Expiration, _ = time.Parse(time.RFC3339, expiration_date)
    jsonToSign, _ := json.Marshal(configNode.Signed)

    hashToSign := sha256.Sum256([]byte(jsonToSign))
    signature, err := rsa.SignPKCS1v15(rng, rsaKey, crypto.SHA256, hashToSign[:])
    if err != nil {
        log.Fatal(err)
        return nil, nil, errors.New("Not possible to sign merkle tree's configuration")
    }
    configNode.Signature = hex.EncodeToString(signature)
    jsonToPublish, _ := json.Marshal(configNode)

    return configNode, jsonToPublish, nil
}

func (this *MerkleTree) AddNewNode(content []byte, _type uint) bool {
    contentNode := new(Node)
    contentNode.IsLeaf = true
    contentNode.Content = content
    contentNode.Type = _type

    contentHash := ethercrypro.Keccak256Hash([]byte(content))
    //contentHash := sha256.Sum256(content)

    contentNode.Hash = hex.EncodeToString(contentHash[:])

    if this.Root == nil {
        this.Root = contentNode
        this.Last = contentNode
        this.Aux = this.Root
        this.List = append(this.List, contentNode)

        return true
    }

    /******************************************************/
    // Finding where to link the new node
    // Looking for a node with free pointers (left or right)
    /******************************************************/
    this.List = append(this.List, contentNode)

    newNode := new(Node)
    newNode.Type = 0

    newNode.Right = contentNode
    contentNode.Parent = newNode
    contentNode.Side = RIGHT_SIDE

    if (this.Root.LeftSize == this.Root.RightSize) || this.Root.IsLeaf {
        newNode.Left = this.Root
        this.Root.Side = LEFT_SIDE
        this.Root.Parent = newNode
        this.Root = newNode
        this.Aux = this.Root
    } else {
        if this.Aux.LeftSize == this.Aux.RightSize {
            newNode.Parent = this.Aux.Parent
            this.Aux.Parent.Right = newNode
            newNode.Side = RIGHT_SIDE
            newNode.Left = this.Aux
            this.Aux.Side = LEFT_SIDE
            this.Aux.Parent = newNode
        } else {
            newNode.Left = this.Aux.Right
            this.Aux.Right.Side = LEFT_SIDE
            newNode.Left.Parent = newNode
            newNode.Parent = this.Aux
            this.Aux.Right = newNode
            newNode.Side = RIGHT_SIDE
        }
        this.Aux = newNode

    }
    ////////////////////////////////////////////////////////

    /******************************************************/
    // Content provider misbehave simulation
    /******************************************************/
    /*
    if len(this.List) == 8 {
      log.Printf("Content provider misbehaving")
      this.List[2].Content[76] = 0x31
      contentHash := ethercrypro.Keccak256Hash([]byte(this.List[2].Content))
      this.List[2].Hash = hex.EncodeToString(contentHash[:])
      p := this.List[2].Parent
      for {
          var buffer bytes.Buffer
          toHash, _ := hex.DecodeString(p.Left.Hash)
          buffer.Write(toHash)
          if p.Right != nil {
              toHash, _ = hex.DecodeString(p.Right.Hash)
              buffer.Write(toHash)
          }

          hash := ethercrypro.Keccak256Hash(buffer.Bytes())
          p.Hash = hex.EncodeToString(hash[:])

          if p.Left != nil {
              p.LeftSize = (p.Left.LeftSize + p.Left.RightSize) + 1
          }
          if p.Right != nil {
              p.RightSize = (p.Right.LeftSize + p.Right.RightSize) + 1
          }
          if p.LeftSize == p.RightSize {this.Aux = p}

          p = p.Parent
          if p == nil {break}
      }
    }
    */
    /******************************************************/
    // Update hash on affected nodes
    /******************************************************/
    p := this.Aux
    for {
        var buffer bytes.Buffer
        toHash, _ := hex.DecodeString(p.Left.Hash)
        buffer.Write(toHash)
        if p.Right != nil {
            toHash, _ = hex.DecodeString(p.Right.Hash)
            buffer.Write(toHash)
        }

        hash := ethercrypro.Keccak256Hash(buffer.Bytes())
        p.Hash = hex.EncodeToString(hash[:])

        if p.Left != nil {
            p.LeftSize = (p.Left.LeftSize + p.Left.RightSize) + 1
        }
        if p.Right != nil {
            p.RightSize = (p.Right.LeftSize + p.Right.RightSize) + 1
        }
        if p.LeftSize == p.RightSize {this.Aux = p}

        p = p.Parent
        if p == nil {break}
    }
    ////////////////////////////////////////////////////////

    return true
}

func (this *MerkleTree) MakeSnapshot() ([]byte, [][32]byte, []uint8, uint, error){
    mkLength := uint(len(this.List))
    if (mkLength < 1) {
        return nil, nil, nil, 0, errors.New("Merkle tree has not been initialized")
    }
    if (mkLength < this.LastCommitted) {
        return nil, nil, nil, 0, errors.New("Merkle tree has no new nodes to commit")
    }
    if (mkLength == this.LastCommitted) {
        log.Print("Merkle tree has no new nodes to commit")
        return nil, nil, nil, 0, nil
    }

    rootHash, _ := hex.DecodeString(this.Root.Hash)
    proofs, sides := this.getProofOfConsistency(this.LastCommitted, 0, mkLength, true)
    this.LastCommitted = mkLength
    return rootHash, proofs, sides, mkLength, nil
}

func (this *MerkleTree) getProofOfConsistency(m uint, first uint, last uint, b bool) ([][32]byte, []uint8) {
    n := last - first;
    k := uint(math.Floor(math.Exp2(math.Ceil(math.Log2(float64(n)) - 1))))
    if m == n {
        if b == true {
            emptyHash := common.Hash{}
            return [][32]byte{emptyHash}, []uint8{0}
        } else {
            return [][32]byte{this.getMTH(first, last)}, []uint8{0}
        }
    } else {
        if k < 1 {
            return [][32]byte{}, []uint8{}
        } else if m <= k {
            proofs, sides := this.getProofOfConsistency(m, first, first + k, b)
            proofs = append(proofs, this.getMTH(first + k, last))
            sides = append(sides, 1)
            return proofs, sides
        } else {
            proofs, sides := this.getProofOfConsistency(m - k, first + k, last, false)
            proofs = append(proofs, this.getMTH(first, first + k))
            sides = append(sides, 0)
            return proofs, sides
        }
    }

}

func (this *MerkleTree) getMTH(first uint, last uint) [32]byte {
    k := uint(math.Ceil(math.Log2(float64(last - first))))
    p := this.List[first]
    for (k > 0) {
        p = p.Parent
        k = k - 1
    }
    if p == nil {
        log.Printf("Null p %d %d", first, last)
    }
    hash, _ := hex.DecodeString(p.Hash)
    return common.BytesToHash(hash)
}

func (this *MerkleTree) GetConfigJSON() []byte {
    var buffer bytes.Buffer
    configJSON, _ := json.Marshal(this.Config)
    buffer.Write(configJSON)
    return buffer.Bytes()[:]
}

func (this *MerkleTree) SearchNode(id string) []byte {
    var buffer bytes.Buffer
    var jsonNode JSONNode

    proofList := make([]Proof, 0, 1)
    jsonNode.Id = id
    jsonNode.TreeLength = uint(len(this.List))

    for i := 0; i < len(this.List); i++ {
        p := this.List[i]
        if p.Type == 1 {
            data := new(Data)
            json.Unmarshal(p.Content, &data)
            if data.Id == id {
                jsonNode.Hash = p.Hash
                jsonNode.Content = data

                /***************************************************/
                // Get elements for proof of existency
                /***************************************************/
                c := 0
                q := p.Parent
                for (q != nil) {
                    c = c + 1
                    var proof Proof
                    if p.Side == RIGHT_SIDE {
                        proof = Proof{LEFT_SIDE, q.Left.Hash}
                    } else {
                        proof = Proof{RIGHT_SIDE, q.Right.Hash}
                    }
                    proofList = append(proofList, proof)
                    p = q
                    q = p.Parent
                }
                jsonNode.Proof = proofList
                log.Printf("%d %d", c, len(this.List))
                /***************************************************/

                _json, _ := json.Marshal(jsonNode)
                buffer.Write(_json)
                break
            }
        }
    }
    return buffer.Bytes()[:]
}

func getChildHashes(node *Node) ([]string) {
    list := make([]string, 0, 1)
    if node == nil {return list}

    left_hashes := getChildHashes(node.Left)
    right_hashes := getChildHashes(node.Right)

    list = append(list, left_hashes...)

    list = append(list, right_hashes...)

    list = append(list, node.Hash)

    return list
}

func (this *MerkleTree) GetJSON() []byte {
    var finalBuffer bytes.Buffer

    nodes := convertToList(this.Root)

    nodesJSON, err := json.Marshal(nodes)
    if err != nil {
        log.Printf("[Error] parsing json for html tree")
    }

    finalBuffer.Write(nodesJSON)

    return finalBuffer.Bytes()[:]
}

func convertToList(node *Node) []*JSONNode {
    list := make([]*JSONNode, 0, 1)
    if node == nil {return list}

    jsonNode := new(JSONNode)
    jsonNode.Id = node.Hash[0:6]
    jsonNode.Hash = node.Hash
    if node.IsLeaf {
        if node.Type == 1 {
            var content Data
            json.Unmarshal(node.Content, &content)
            jsonNode.Content = content
        } else {
            var config Config
            json.Unmarshal(node.Content, &config)
            jsonNode.Content = config
        }
        jsonNode.Type = node.Type

    } else {
        jsonNode.Type = 0
    }
    if node.Parent != nil {
        jsonNode.IdParent = node.Parent.Hash[0:6]
    }

    list = append(list, jsonNode)
    left_list := convertToList(node.Left)
    right_list := convertToList(node.Right)

    list = append(list, left_list...)
    list = append(list, right_list...)

    return list
}
