package merkletree

type Data struct {
	Id           string    `json:"id"`
	Local        string    `json:"local"`
	Visitor      string    `json:"visitor"`
	LocalGoals   int       `json:"localGoals"`
	VisitorGoals int       `json:"visitorGoals"`
}
