package merkletree

import (
    "time"
)

type Config struct {
    Signed struct {
        URL string `json:"url"`
        SmartContractAddress string `json:"sc_address"`
        Frequency string `json:"frequency"`
        Expiration time.Time `json:"expiration"`
        DataStructure string `json:"data_structure"`
    } `json:"signed"`
    Signature string `json:"signature"`
    Notes string `json:"notes"`
    //PublicCert string `json:"public_cert"`
}
