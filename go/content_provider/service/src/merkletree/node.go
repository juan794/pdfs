package merkletree

type Node struct {
    Id uint64
    Hash string
    Content []byte
    Type uint
    Side uint

    IsLeaf bool
    Left, Right, Parent *Node
    LeftSize, RightSize uint64
}

const LEFT_SIDE = 0
const RIGHT_SIDE = 1
