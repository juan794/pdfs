package web

import (
    "log"
    "net/http"
    "html/template"

    "merkletree"

)

type Service struct {
    Mk *merkletree.MerkleTree
}

var this *Service

func (service *Service) Server(certificateFile string, keyFile string){
    this = service

    http.HandleFunc("/tree.json", printTreeJSON)
    http.HandleFunc("/tree.html", drawTreeGraph)
    http.HandleFunc("/config.json", writeConfigJSON)
    http.HandleFunc("/fixtures/", writeFixture)
	err := http.ListenAndServeTLS(":443", certificateFile, keyFile, nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}

func writeFixture(w http.ResponseWriter, r *http.Request) {
    id := r.URL.Path[len("/fixtures/"):]

    w.Header().Set("Content-Type", "application/json")
    w.Write(this.Mk.SearchNode(id))
}

func writeConfigJSON(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.Write(this.Mk.GetConfigJSON())
}

func drawTreeGraph(w http.ResponseWriter, r *http.Request) {
    t, err := template.ParseFiles("../web/assets/tree.html")
    if err != nil {
        log.Fatal("Error parsing template", err)
        return
    }
    t.Execute(w, nil)
}

func printTreeJSON(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.Write(this.Mk.GetJSON())
}
