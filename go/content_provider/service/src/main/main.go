package main

import (
    "os"
    "fmt"
    "math"
    "flag"
    "strconv"
    "encoding/json"

    "merkletree"
    "feed"
    "web"
)

func main() {
    var keyFile string
    var certFile string
    var url string
    var token string
    var scAddress string
    var expirationDate string
    var ipcFile string
    var etherAccFile string
    var etherPassword string

    flag.StringVar(&keyFile, "key_file", "ABSOLUTE PATH", "Private key (PEM format)")
    flag.StringVar(&certFile, "cert_file", "ABSOLUTE PATH", "x509 certificate/public key")
    flag.StringVar(&url, "url", "https://api.football-data.org/v1/competitions/464/fixtures", "Service URL/JSON API")
    flag.StringVar(&token, "token", "KEY VALUE", "Key/Token to consume API in https://api.football-data.org")
    flag.StringVar(&scAddress, "sc_address", "0x....", "Content Contract Address (e.g., 0xfff....")
    flag.StringVar(&expirationDate, "expiration_date", "2018-05-28T00:00:00Z", "Date where merkle tree will expires (ISO 8601 format)")
    flag.StringVar(&ipcFile, "ipc_file", "Absolute path to the geth.ipc", "IPC file from Geth (Ethereum client)")
    flag.StringVar(&etherAccFile, "ethereum_account", "Absoulte path private key for ethereum", "Ethereum account's private key file")
    flag.StringVar(&etherPassword, "ether_password", "000", "Ethereum account's password")
    flag.Parse()

    mk, err := merkletree.Init(keyFile, certFile, url, scAddress, expirationDate)
    if err != nil {
        fmt.Println("Merkle tree has not been initialized")
        os.Exit(1)
    }
    fmt.Println("Merkle tree has been initialized")
    feed := feed.NewFeed(url, token, mk, ipcFile, scAddress, etherAccFile, etherPassword)
    feed.SubmitNewRoot()
    feed.FeedMerkleTree()

    server := new(web.Service)
    server.Mk = mk
    fmt.Println("Web server has been initialized")
    server.Server(certFile, keyFile)

    os.Exit(0)
}
