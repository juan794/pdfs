package feed

import (
    "merkletree"

    "log"
    "fmt"
    "time"
    "strings"
    "net/http"
    "io/ioutil"
    "database/sql"
    "encoding/json"

    _ "github.com/go-sql-driver/mysql"

    "github.com/ethereum/go-ethereum/common"
    "github.com/ethereum/go-ethereum/ethclient"
    "github.com/ethereum/go-ethereum/accounts/abi/bind"
)

type Feed struct {
    Url string
    Token string
    IpcFile string
    EtherAccFile string
    EtherPassword string
    SmartContractAddress string
    Mk *merkletree.MerkleTree
}

func NewFeed(url string, token string, mk *merkletree.MerkleTree, ipcFile string, smartContractAddress string, etherAccFile string, etherPassword string) *Feed {
    feed := new(Feed)
    feed.Url = url
    feed.Token = token
    feed.Mk = mk
    feed.IpcFile = ipcFile
    feed.SmartContractAddress = smartContractAddress
    feed.EtherAccFile = etherAccFile
    feed.EtherPassword = etherPassword

    return feed
}

func (this *Feed) FeedMerkleTree() {
    resp, _ := http.Get(this.Url)
    resp.Header.Set("X-Auth-Token", this.Token)
    defer resp.Body.Close()
    body, _ := ioutil.ReadAll(resp.Body)

    /************************************************
     * Loading RESTFul data into Golang structure
     * Note: This example is in a Soccer context
     ***********************************************/
    header := new(Header)
    json.Unmarshal(body, &header)
    /***********************************************/

    /************************************************
     * Extracting matches information and put them
     * into the Merkle tree
     * Constraints:
     * - Status is "FINISHED"
     * - Date after last updated item's date
     ***********************************************/
    var lastDate time.Time
    status := "FINISHED"
    lastMatchday := 1
    for _, data := range header.Fixtures {
        /**
         * Dynamic proof of consistency evaluation
         */
        if lastMatchday != data.Matchday {
            lastMatchday = data.Matchday
            this.SubmitNewRoot()
        }

        if data.Status == status && data.Date.After(lastDate) {
            merkleData := new(merkletree.Data)
            merkleData.Id = strings.Replace(data.Links.Self.Href, "http://api.football-data.org/v1/fixtures/", "", -1)
            merkleData.Local = data.HomeTeamName
            merkleData.Visitor = data.AwayTeamName
            merkleData.LocalGoals = data.Result.GoalsHomeTeam
            merkleData.VisitorGoals = data.Result.GoalsAwayTeam

            _json, _ := json.Marshal(merkleData)
            this.Mk.AddNewNode(_json, 1)
        }
    }
    /**********************************************/
    this.SubmitNewRoot()
}

func (this *Feed) getEthereumConn() (*ethclient.Client, *bind.TransactOpts, *Provider, *Provider, error) {
    conn, err := ethclient.Dial(this.IpcFile)
    if err != nil {
        log.Fatal("Failed to connect to the Ethereum client: %v", err)
        return nil, nil, nil, nil, err
    }
    keyFile, _ := ioutil.ReadFile(this.EtherAccFile)
    keyContent := string(keyFile)
    auth, err := bind.NewTransactor(strings.NewReader(keyContent), this.EtherPassword)
    if err != nil {
        log.Fatal("Failed to create authorized transactor: %v", err)
        return nil, nil, nil, nil, err
    }

    token, err := NewProvider(common.HexToAddress("0x5c6De721F6F8C21e313C71090b8DBD646CC9689D"), conn)
    if err != nil {
        log.Fatal("Failed to instantiate a Token contract: %v", err)
        return nil, nil, nil, nil, err
    }

    return conn, auth, token, nil
}

func (this *Feed) ResetSmartContract() (error) {
    _, auth, token, err := this.getEthereumConn()
    if err != nil {
        log.Fatal("Failed to instantiate a Token contract: %v", err)
        return err
    }
    token.ResetData(auth)
    return nil
}

func (this *Feed) SubmitNewRoot() {
    root, proofs, sides, newNodes, err := this.Mk.MakeSnapshot()
    if err != nil {
        log.Fatal("Error submitting root: %v", err)
    }

    if root != nil {
        _, auth, token, err := this.getEthereumConn()
        if err != nil {
            log.Fatal("Failed to instantiate a Token contract: %v", err)
        }
        tx, err := token.CommitNewRoot(auth, common.BytesToHash(root), proofs, sides)
        if err != nil {
    	    log.Fatalf("Failure submitting new root: %v", err)
        }
        fmt.Printf("Tx: %s, Type: %d, proofs length: %d, new nodes: %d\n", tx1.Hash().String(), 1, len(proofs), newNodes)

        time.Sleep(time.Second * 8)
    }
}
