// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package feed

import (
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

// ProviderABI is the input ABI used to generate the binding from.
const ProviderABI = "[{\"constant\":true,\"inputs\":[],\"name\":\"getValidated\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"hash\",\"type\":\"bytes32\"},{\"name\":\"proofs\",\"type\":\"bytes32[]\"},{\"name\":\"sides\",\"type\":\"uint8[]\"}],\"name\":\"validateProofOfConsistency\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"resetData\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"hash\",\"type\":\"bytes32\"},{\"name\":\"proofs\",\"type\":\"bytes32[]\"},{\"name\":\"sides\",\"type\":\"uint8[]\"}],\"name\":\"commitNewRoot\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getLastUpdateTime\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"json\",\"type\":\"string\"},{\"name\":\"proofs\",\"type\":\"bytes32[]\"},{\"name\":\"sides\",\"type\":\"uint8[]\"}],\"name\":\"validateProofOfExistence\",\"outputs\":[{\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"proofs\",\"type\":\"bytes32[]\"},{\"name\":\"sides\",\"type\":\"uint8[]\"},{\"name\":\"leaf\",\"type\":\"bytes32\"}],\"name\":\"getMTH\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"},{\"name\":\"\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"pure\",\"type\":\"function\"},{\"inputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"}]"

// Provider is an auto generated Go binding around an Ethereum contract.
type Provider struct {
	ProviderCaller     // Read-only binding to the contract
	ProviderTransactor // Write-only binding to the contract
	ProviderFilterer   // Log filterer for contract events
}

// ProviderCaller is an auto generated read-only Go binding around an Ethereum contract.
type ProviderCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProviderTransactor is an auto generated write-only Go binding around an Ethereum contract.
type ProviderTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProviderFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type ProviderFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// ProviderSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type ProviderSession struct {
	Contract     *Provider         // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// ProviderCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type ProviderCallerSession struct {
	Contract *ProviderCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// ProviderTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type ProviderTransactorSession struct {
	Contract     *ProviderTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// ProviderRaw is an auto generated low-level Go binding around an Ethereum contract.
type ProviderRaw struct {
	Contract *Provider // Generic contract binding to access the raw methods on
}

// ProviderCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type ProviderCallerRaw struct {
	Contract *ProviderCaller // Generic read-only contract binding to access the raw methods on
}

// ProviderTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type ProviderTransactorRaw struct {
	Contract *ProviderTransactor // Generic write-only contract binding to access the raw methods on
}

// NewProvider creates a new instance of Provider, bound to a specific deployed contract.
func NewProvider(address common.Address, backend bind.ContractBackend) (*Provider, error) {
	contract, err := bindProvider(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Provider{ProviderCaller: ProviderCaller{contract: contract}, ProviderTransactor: ProviderTransactor{contract: contract}, ProviderFilterer: ProviderFilterer{contract: contract}}, nil
}

// NewProviderCaller creates a new read-only instance of Provider, bound to a specific deployed contract.
func NewProviderCaller(address common.Address, caller bind.ContractCaller) (*ProviderCaller, error) {
	contract, err := bindProvider(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &ProviderCaller{contract: contract}, nil
}

// NewProviderTransactor creates a new write-only instance of Provider, bound to a specific deployed contract.
func NewProviderTransactor(address common.Address, transactor bind.ContractTransactor) (*ProviderTransactor, error) {
	contract, err := bindProvider(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &ProviderTransactor{contract: contract}, nil
}

// NewProviderFilterer creates a new log filterer instance of Provider, bound to a specific deployed contract.
func NewProviderFilterer(address common.Address, filterer bind.ContractFilterer) (*ProviderFilterer, error) {
	contract, err := bindProvider(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &ProviderFilterer{contract: contract}, nil
}

// bindProvider binds a generic wrapper to an already deployed contract.
func bindProvider(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(ProviderABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Provider *ProviderRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Provider.Contract.ProviderCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Provider *ProviderRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Provider.Contract.ProviderTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Provider *ProviderRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Provider.Contract.ProviderTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Provider *ProviderCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Provider.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Provider *ProviderTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Provider.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Provider *ProviderTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Provider.Contract.contract.Transact(opts, method, params...)
}

// GetLastUpdateTime is a free data retrieval call binding the contract method 0xb7f88320.
//
// Solidity: function getLastUpdateTime() constant returns(uint256)
func (_Provider *ProviderCaller) GetLastUpdateTime(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _Provider.contract.Call(opts, out, "getLastUpdateTime")
	return *ret0, err
}

// GetLastUpdateTime is a free data retrieval call binding the contract method 0xb7f88320.
//
// Solidity: function getLastUpdateTime() constant returns(uint256)
func (_Provider *ProviderSession) GetLastUpdateTime() (*big.Int, error) {
	return _Provider.Contract.GetLastUpdateTime(&_Provider.CallOpts)
}

// GetLastUpdateTime is a free data retrieval call binding the contract method 0xb7f88320.
//
// Solidity: function getLastUpdateTime() constant returns(uint256)
func (_Provider *ProviderCallerSession) GetLastUpdateTime() (*big.Int, error) {
	return _Provider.Contract.GetLastUpdateTime(&_Provider.CallOpts)
}

// GetMTH is a free data retrieval call binding the contract method 0xf940eb90.
//
// Solidity: function getMTH(proofs bytes32[], sides uint8[], leaf bytes32) constant returns(bytes32, bytes32)
func (_Provider *ProviderCaller) GetMTH(opts *bind.CallOpts, proofs [][32]byte, sides []uint8, leaf [32]byte) ([32]byte, [32]byte, error) {
	var (
		ret0 = new([32]byte)
		ret1 = new([32]byte)
	)
	out := &[]interface{}{
		ret0,
		ret1,
	}
	err := _Provider.contract.Call(opts, out, "getMTH", proofs, sides, leaf)
	return *ret0, *ret1, err
}

// GetMTH is a free data retrieval call binding the contract method 0xf940eb90.
//
// Solidity: function getMTH(proofs bytes32[], sides uint8[], leaf bytes32) constant returns(bytes32, bytes32)
func (_Provider *ProviderSession) GetMTH(proofs [][32]byte, sides []uint8, leaf [32]byte) ([32]byte, [32]byte, error) {
	return _Provider.Contract.GetMTH(&_Provider.CallOpts, proofs, sides, leaf)
}

// GetMTH is a free data retrieval call binding the contract method 0xf940eb90.
//
// Solidity: function getMTH(proofs bytes32[], sides uint8[], leaf bytes32) constant returns(bytes32, bytes32)
func (_Provider *ProviderCallerSession) GetMTH(proofs [][32]byte, sides []uint8, leaf [32]byte) ([32]byte, [32]byte, error) {
	return _Provider.Contract.GetMTH(&_Provider.CallOpts, proofs, sides, leaf)
}

// GetValidated is a free data retrieval call binding the contract method 0x33ffe30d.
//
// Solidity: function getValidated() constant returns(bool)
func (_Provider *ProviderCaller) GetValidated(opts *bind.CallOpts) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Provider.contract.Call(opts, out, "getValidated")
	return *ret0, err
}

// GetValidated is a free data retrieval call binding the contract method 0x33ffe30d.
//
// Solidity: function getValidated() constant returns(bool)
func (_Provider *ProviderSession) GetValidated() (bool, error) {
	return _Provider.Contract.GetValidated(&_Provider.CallOpts)
}

// GetValidated is a free data retrieval call binding the contract method 0x33ffe30d.
//
// Solidity: function getValidated() constant returns(bool)
func (_Provider *ProviderCallerSession) GetValidated() (bool, error) {
	return _Provider.Contract.GetValidated(&_Provider.CallOpts)
}

// ValidateProofOfConsistency is a free data retrieval call binding the contract method 0x384daf62.
//
// Solidity: function validateProofOfConsistency(hash bytes32, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderCaller) ValidateProofOfConsistency(opts *bind.CallOpts, hash [32]byte, proofs [][32]byte, sides []uint8) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Provider.contract.Call(opts, out, "validateProofOfConsistency", hash, proofs, sides)
	return *ret0, err
}

// ValidateProofOfConsistency is a free data retrieval call binding the contract method 0x384daf62.
//
// Solidity: function validateProofOfConsistency(hash bytes32, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderSession) ValidateProofOfConsistency(hash [32]byte, proofs [][32]byte, sides []uint8) (bool, error) {
	return _Provider.Contract.ValidateProofOfConsistency(&_Provider.CallOpts, hash, proofs, sides)
}

// ValidateProofOfConsistency is a free data retrieval call binding the contract method 0x384daf62.
//
// Solidity: function validateProofOfConsistency(hash bytes32, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderCallerSession) ValidateProofOfConsistency(hash [32]byte, proofs [][32]byte, sides []uint8) (bool, error) {
	return _Provider.Contract.ValidateProofOfConsistency(&_Provider.CallOpts, hash, proofs, sides)
}

// ValidateProofOfExistence is a free data retrieval call binding the contract method 0xdaf44e59.
//
// Solidity: function validateProofOfExistence(json string, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderCaller) ValidateProofOfExistence(opts *bind.CallOpts, json string, proofs [][32]byte, sides []uint8) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _Provider.contract.Call(opts, out, "validateProofOfExistence", json, proofs, sides)
	return *ret0, err
}

// ValidateProofOfExistence is a free data retrieval call binding the contract method 0xdaf44e59.
//
// Solidity: function validateProofOfExistence(json string, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderSession) ValidateProofOfExistence(json string, proofs [][32]byte, sides []uint8) (bool, error) {
	return _Provider.Contract.ValidateProofOfExistence(&_Provider.CallOpts, json, proofs, sides)
}

// ValidateProofOfExistence is a free data retrieval call binding the contract method 0xdaf44e59.
//
// Solidity: function validateProofOfExistence(json string, proofs bytes32[], sides uint8[]) constant returns(bool)
func (_Provider *ProviderCallerSession) ValidateProofOfExistence(json string, proofs [][32]byte, sides []uint8) (bool, error) {
	return _Provider.Contract.ValidateProofOfExistence(&_Provider.CallOpts, json, proofs, sides)
}

// CommitNewRoot is a paid mutator transaction binding the contract method 0x586db0f8.
//
// Solidity: function commitNewRoot(hash bytes32, proofs bytes32[], sides uint8[]) returns()
func (_Provider *ProviderTransactor) CommitNewRoot(opts *bind.TransactOpts, hash [32]byte, proofs [][32]byte, sides []uint8) (*types.Transaction, error) {
	return _Provider.contract.Transact(opts, "commitNewRoot", hash, proofs, sides)
}

// CommitNewRoot is a paid mutator transaction binding the contract method 0x586db0f8.
//
// Solidity: function commitNewRoot(hash bytes32, proofs bytes32[], sides uint8[]) returns()
func (_Provider *ProviderSession) CommitNewRoot(hash [32]byte, proofs [][32]byte, sides []uint8) (*types.Transaction, error) {
	return _Provider.Contract.CommitNewRoot(&_Provider.TransactOpts, hash, proofs, sides)
}

// CommitNewRoot is a paid mutator transaction binding the contract method 0x586db0f8.
//
// Solidity: function commitNewRoot(hash bytes32, proofs bytes32[], sides uint8[]) returns()
func (_Provider *ProviderTransactorSession) CommitNewRoot(hash [32]byte, proofs [][32]byte, sides []uint8) (*types.Transaction, error) {
	return _Provider.Contract.CommitNewRoot(&_Provider.TransactOpts, hash, proofs, sides)
}

// ResetData is a paid mutator transaction binding the contract method 0x478c4e0e.
//
// Solidity: function resetData() returns()
func (_Provider *ProviderTransactor) ResetData(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Provider.contract.Transact(opts, "resetData")
}

// ResetData is a paid mutator transaction binding the contract method 0x478c4e0e.
//
// Solidity: function resetData() returns()
func (_Provider *ProviderSession) ResetData() (*types.Transaction, error) {
	return _Provider.Contract.ResetData(&_Provider.TransactOpts)
}

// ResetData is a paid mutator transaction binding the contract method 0x478c4e0e.
//
// Solidity: function resetData() returns()
func (_Provider *ProviderTransactorSession) ResetData() (*types.Transaction, error) {
	return _Provider.Contract.ResetData(&_Provider.TransactOpts)
}
