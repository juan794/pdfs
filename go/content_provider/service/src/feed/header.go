package feed

import (
    "time"
)

type Header struct {
	Links struct {
		Self struct {
			Href string `json:"href"`
		} `json:"self"`
		Competition struct {
			Href string `json:"href"`
		} `json:"competition"`
	} `json:"_links"`
	Count    int           `json:"count"`
	Fixtures []struct {
        Links struct {
            Self struct {
                Href string `json:"href"`
            } `json:"self"`
            Competition struct {
                Href string `json:"href"`
            } `json:"competition"`
            HomeTeam struct {
                Href string `json:"href"`
            } `json:"homeTeam"`
            AwayTeam struct {
                Href string `json:"href"`
            } `json:"awayTeam"`
        } `json:"_links"`
        Date time.Time `json:"date"`
        Status string `json:"status"`
        Matchday int `json:"matchday"`
        HomeTeamName string `json:"homeTeamName"`
        AwayTeamName string `json:"awayTeamName"`
        Result struct {
            GoalsHomeTeam int `json:"goalsHomeTeam"`
            GoalsAwayTeam int `json:"goalsAwayTeam"`
            HalfTime struct {
                GoalsHomeTeam int `json:"goalsHomeTeam"`
                GoalsAwayTeam int `json:"goalsAwayTeam"`
            } `json:"halfTime"`
        } `json:"result"`
        Odds interface{} `json:"odds"`
    } `json:"fixtures"`
}
