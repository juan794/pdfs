pragma solidity ^0.4.21;

contract Provider {

    bytes32 root;
    uint256 lasUpdateTime = 0;

    bool validated;

    function Provider() public {}

    function commitNewRoot(bytes32 hash, bytes32[] proofs, uint8[] sides)
    public {
        validated = validateProofOfConsistency(hash, proofs, sides);
        if (validated) {
            lasUpdateTime = now;
            root = hash;
        }
    }

    function validateProofOfExistence(string json, bytes32[] proofs, uint8[] sides)
    view public returns(bool) {
        bytes32 ignored;
        bytes32 hash = keccak256(json);

        (hash, ignored) = getMTH(proofs, sides, hash);

        return (hash == root);
    }

    function validateProofOfConsistency(bytes32 hash, bytes32[] proofs, uint8[] sides)
    view public returns(bool) {
        bytes32 hash_new;
        bytes32 hash_old;
        if (lasUpdateTime == 0) {return true;}
        if (proofs[0] == 0x00) {proofs[0] = root;}

        (hash_new, hash_old) = getMTH(proofs, sides, 0x00);

        return (hash_new == hash) && (hash_old == root);
    }

    function getMTH(bytes32[] proofs, uint8[] sides, bytes32 leaf)
    pure public returns(bytes32, bytes32) {
        uint i = 0;
        bytes32 hash_new = leaf;
        bytes32 hash_old = leaf;
        if (leaf == 0x00) {
            i = 1;
            hash_new = proofs[0];
            hash_old = proofs[0];
        }
        for (i; i < proofs.length; i++) {
            if (sides[i] == 1) {
                hash_new = keccak256(hash_new, proofs[i]);
            } else {
                hash_old = keccak256(proofs[i], hash_old);
                hash_new = keccak256(proofs[i], hash_new);
            }
        }
        return (hash_new, hash_old);
    }

    function resetData() public {
        root = 0x00;
        lasUpdateTime = 0;
    }

    function getLastUpdateTime() view public returns(uint256) {
        return lasUpdateTime;
    }

    function getValidated() view public returns(bool) {
        return validated;
    }

}
