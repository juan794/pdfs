module.exports = {
    networks: {
        local: {
            host: "localhost",
            port: 8545,
            network_id: "*",
            from: 0xda0ae181a014c8c4ebe5cfd18459188039c16f31
        }
    }
};
